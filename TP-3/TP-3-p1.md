## TP3 : Services

### I. Service SSH

**🌞 S'assurer que le service sshd est démarré** 

    [matheo@node1 ~]$ systemctl status sshd
    ● sshd.service - OpenSSH server daemon
     Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; preset: enabled)
     Active: active (running) since Mon 2024-01-29 15:57:20 CET; 4min 1s ago
       Docs: man:sshd(8)
             man:sshd_config(5)
    Main PID: 683 (sshd)
      Tasks: 1 (limit: 4606)
     Memory: 6.4M
        CPU: 38ms
     CGroup: /system.slice/sshd.service
             └─683 "sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups"  

**🌞 Analyser les processus liés au service SSH**

    [matheo@node1 ~]$ ps -A | grep sshd
     683 ?        00:00:00 sshd
     1267 ?        00:00:00 sshd
     1280 ?        00:00:00 sshd

**🌞 Déterminer le port sur lequel écoute le service SSH**

    [matheo@node1 ~]$ sudo ss -alnpt | grep ssh
    LISTEN 0      128          0.0.0.0:22        0.0.0.0:*    users:(("sshd",pid=683,fd=3))
    LISTEN 0      128             [::]:22           [::]:*    users:(("sshd",pid=683,fd=4))  

**🌞 Consulter les logs du service SSH**

    [matheo@node1 ssh]$ journalctl -xe -u sshd -f  
    Jan 29 15:58:50 node1.tp3.b1 sshd[1267]: main: sshd: ssh-rsa algorithm is disabled
    Jan 29 15:58:50 node1.tp3.b1 sshd[1267]: Accepted publickey for matheo from 10.3.1.1 port 56740 ssh2: RSA SHA256:H3DWN6qjrysozewnuWi22hchx/sLrsEHhum7Emk+B+0
    Jan 29 15:58:50 node1.tp3.b1 sshd[1267]: pam_unix(sshd:session): session opened for user matheo(uid=1000) by (uid=0)  

### 2. Modification du service  

**🌞 Identifier le fichier de configuration du serveur SSH**  

    [matheo@node1 ssh]$ sudo cat /etc/ssh/sshd_config   

**🌞 Modifier le fichier de conf**

    [matheo@node1 ssh]$ echo $RANDOM
    9999  

    [matheo@node1 ssh]$ sudo nano /etc/ssh/sshd_config | grep Port
    Port 9999

    [matheo@node1 ~]$ sudo firewall-cmd --list-all | grep ports
    ports: 9999/tcp
    
**🌞 Redémarrer le service**  
    [matheo@node1 ssh]$sudo systemctl restart sshd
    
**🌞 Effectuer une connexion SSH sur le nouveau port**
    
    PS C:\Users\mathi> ssh -p 9999 matheo@10.3.1.11
    Last login: Tue Jan 30 14:51:05 2024
    [matheo@node1 ~]$
## II. Service HTTP  

### 1. Mise en place  

**🌞 Installer le serveur NGINX**

    [matheo@node1 ~]$ dnf install nginx  

**🌞 Démarrer le service NGINX**

    [matheo@node1 ~]$ sudo systemctl start nginx   

**🌞 Déterminer sur quel port tourne NGINX**

    [matheo@node1 ~]$ sudo ss -alnpt | grep nginx
    LISTEN 0      511          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=11310,fd=6),("nginx",pid=11309,fd=6))
    LISTEN 0      511             [::]:80           [::]:*    users:(("nginx",pid=11310,fd=7),("nginx",pid=11309,fd=7))

    [matheo@node1 ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
    success


**🌞 Déterminer les processus liés au service NGINX**

    [matheo@node1 ~]$ sudo ps -ef | grep nginx       11309       1  0 15:16 ?        00:00:00 nginx: master process /usr/sbin/nginx
    nginx      11310   11309  0 15:16 ?        00:00:00 nginx: worker process 

**🌞 Déterminer le nom de l'utilisateur qui lance NGINX**

    nginx      11310   11309  0 15:16 ?        00:00:00 nginx: worker process

    [matheo@node1 ~]$ cat /etc/passwd | grep matheo
    matheo:x:1000:1000:matheo:/home/matheo:/bin/bash

**🌞 Test !**

```bash
    mathi@MSI MINGW64 ~
$ curl 10.3.1.11
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0<!doctype html>
 <html>
  <head>
    <meta charset='utf-8'>
     <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
```

### 2. Analyser la conf de NGINX

**🌞 Déterminer le path du fichier de configuration de NGINX**

    [matheo@node1 ~]$ ls -al /etc/nginx/nginx.conf
    -rw-r--r--. 1 root root 2334 Oct 16 20:00 /etc/nginx/nginx.conf


**🌞 Trouver dans le fichier de conf**

```shell
[matheo@node1 ~]$ cat /etc/nginx/nginx.conf | grep ' server' -A 16
    server {
        listen       80;
        listen       [::]:80;
        server_name  _;
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        error_page 404 /404.html;
        location = /404.html {
        }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }
    }
```
**🌞 Créer un site web**

    [matheo@node1 tp3_linux]$ cat index.html
    <h1>MEOW mon premier serveur web</h1>

**🌞 Gérer les permissions**


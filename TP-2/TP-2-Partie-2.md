## Partie 2 : Programmes et paquets

## I. Programmes et processus

**1. Run then kill**

1. 🌞 Lancer un processus sleep

```shell
[matheo@john ~]$ pidof sleep
1420
```

2. 🌞 Terminez le processus sleep depuis le deuxième terminal
```shell
[matheo@john ~]$ kill 1420
```  

### 2. Tâche de fond

1. 🌞 Lancer un nouveau processus sleep, mais en tâche de fond

```shell
[matheo@john ~]$ sleep 1000&
[1] 1446
```

2. 🌞 Visualisez la commande en tâche de fond  

```shell
[matheo@john ~]$ jobs -p
1446
```

### 3. Find paths

1. 🌞 Trouver le chemin où est stocké le programme sleep  

```shell
[matheo@john ~]$ whereis sleep
sleep: /usr/bin/sleep
```
```shell
[matheo@john ~]$ ls -al /usr/bin/sleep | grep sleep
-rwxr-xr-x. 1 root root 36312 Apr 24  2023 /usr/bin/sleep
```

2. 🌞 Tant qu'on est à chercher des chemins : trouver les chemins vers tous les fichiers qui s'appellent .bashrc

```shell
[matheo@john ~]$ sudo find / -name "*.bashrc"
[sudo] password for matheo:
/etc/skel/.bashrc
/root/.bashrc
/home/matheo/.bashrc
/home/papier_alu/.bashrc
```

### 4. La variable PATH

🌞 Vérifier que :  
les commandes sleep, ssh, et ping sont bien des programmes stockés dans l'un des dossiers listés dans votre PATH

**path**
```shell
[matheo@john ~]$ echo $PATH
/home/matheo/.local/bin:/home/matheo/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin
```

**sleep**
```shell
[matheo@john ~]$ which sleep
/usr/bin/sleep
```

**ssh**
```shell
[matheo@john ~]$ which ssh
/usr/bin/ssh
```

**ping**
```shell
[matheo@john ~]$ which ping
/usr/bin/ping
```

## II. Paquets

1. 🌞 Installer le paquet firefox (git)

```shell
[matheo@john ~]$ sudo dnf install git-all
```

2. 🌞 Utiliser une commande pour lancer Firefox  (git)

**path**
```shell
[matheo@john ~]$ which git
/usr/bin/git
```
```shell
[matheo@john ~]$ git
usage: git ...
```

3. 🌞 Installer le paquet nginx

```shell
[matheo@john ~]$ sudo yum install nginx  
```

4. 🌞 Déterminer

**logs de NGINX**
```shell
/var/log/nginx/
```

**configuration de NGINX**
```shell
[matheo@john ~]$ sudo find / -name "nginx.conf"   
/etc/nginx/nginx.conf
```

5. 🌞 Mais aussi déterminer...  

```shell
[matheo@john ~]$ sudo find / -name "URL*"
/usr/share/perl5/URI/URL.pm
```



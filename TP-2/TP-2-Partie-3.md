## Partie 3 : Poupée russe

1. 🌞 Récupérer le fichier meow  

**pour dl un dossier depuis un URL**
```shell
[matheo@john ~]$ sudo dnf install wget  
```
**le dl**
```shell
[matheo@john ~]$ wget https://gitlab.com/it4lik/b1-linux-2023/-/raw/master/tp/2/meow
```  


2. 🌞 Trouver le dossier dawa/

**file**
```shell
[matheo@john ~]$ file /home/matheo/meow
/home/matheo/meow: Zip archive data, at least v2.0 to extract
```

**unzip**
```shell
[matheo@john ~]$ sudo dnf install unzip
```
```shell
[matheo@john ~]$ unzip meow
```
**XZ**
```shell
[matheo@john ~]$ file /home/matheo/meow
/home/matheo/meow: XZ compressed data
```
```shell
[matheo@john ~]$ dnf install xz
```
```shell
[matheo@john ~]$ mv meow meow.xz
```
```shell
[matheo@john ~]$ xz -d meow.xz
```
**bzip2**
```shell
[matheo@john ~]$ file /home/matheo/meow
/home/matheo/meow: bzip2 compressed data, block size = 900k  
```
```shell
[matheo@john ~]$ dnf install bzip2
```
```shell
[matheo@john ~]$ mv meow meow.bz2
[matheo@john ~]$ bzip2 -d meow.bz2
```
**RAR**
```shell
[matheo@john ~]$ file /home/matheo/meow
/home/matheo/meow: RAR archive data, v5  
```
```shell
[matheo@john ~]$ dnf install unar 
```
```shell
[matheo@john ~]$ mv meow meow.rar
[matheo@john ~]$ unar -f meow.rar
```

**gzip**
```shell
[matheo@john ~]$ file meow
meow: gzip compressed data, from Unix
```


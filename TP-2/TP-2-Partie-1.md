## I. Fichiers

### 1. Find me

1. 🌞 Trouver le chemin vers le répertoire personnel de votre utilisateur   
```shell
[matheo@john ~]$ cd /home/matheo
```

2. 🌞 Trouver le chemin du fichier de logs SSH  
```shell
[matheo@john ~]$ sudo cat /var/log/secure
```

3. 🌞 Trouver le chemin du fichier de configuration du serveur SSH  
```shell
[matheo@john ~]$ sudo cat /etc/ssh/sshd_config
```


## II. Users

### 1. Nouveau user

1. 🌞 Créer un nouvel utilisateur  

**user :**
```shell
[matheo@john ~]$ useradd marmotte  
```
**mot de passe :**
```shell
[matheo@john ~]$ sudo passwd marmotte
Changing password for user marmotte.
New password: Chocolat  
```
**papier_alu :**
```shell
[matheo@john ~]$ sudo usermod -d /home/papier_alu/ -m marmotte  
```

### 2. Infos enregistrées par le système

1. 🌞 Prouver que cet utilisateur a été créé  
```shell
[matheo@john ~]$ sudo cat /etc/passwd | grep marmotte
marmotte:x:1001:1001::/home/papier_alu/:/bin/bash  
```
2. 🌞 Déterminer le hash du password de l'utilisateur marmotte  
```shell
[matheo@john ~]$ sudo cat /etc/shadow | grep marmotte
marmotte:$6$Svu/VhDxsAI7FVUa$6tDSNx6qGInGdGTieevTfd0v7dWn.kgIplILNrxOKbQaSq5kHBKWIoXFI3VXt9Z2Tdw.y7Ym3F3G.Dq3WNpmm.:19744:0:99999:7:::  
```
### 3. Connexion sur le nouvel utilisateur

1. 🌞 Tapez une commande pour vous déconnecter : fermer votre session utilisateur  

       ctrl + d  

2. 🌞 Assurez-vous que vous pouvez vous connecter en tant que l'utilisateur marmotte 

**login :**
```shell
login:marmotte  
password:  
Last login: Tue Jan 23 15:07:47 2024 from 10.6.1.1  
[marmotte@john ~]$  
```
**denied :**
```shell
[marmotte@john ~]$ ls /home/matheo/
ls: cannot open directory '/home/matheo/': Permission denied  
```
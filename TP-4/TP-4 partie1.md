**🌞 Partitionner le disque à l'aide de LVM**

créer 2 physical volumes (PV) :  

    [matheo@storage ~]$ lsblk
    NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
    sda           8:0    0    8G  0 disk
    ├─sda1        8:1    0    1G  0 part /boot
    └─sda2        8:2    0    7G  0 part
    ├─rl-root 253:0    0  6.2G  0 lvm  /
    └─rl-swap 253:1    0  820M  0 lvm  [SWAP]
    sdb           8:16   0    2G  0 disk
    sdc           8:32   0    2G  0 disk
    sr0          11:0    1 1024M  0 rom  


    [matheo@storage ~]$ sudo pvcreate /dev/sdb
    Physical volume "/dev/sdb" successfully created.
    [matheo@storage ~]$ sudo pvcreate /dev/sdc
    Physical volume "/dev/sdc" successfully created.

créer un nouveau volume group (VG) :

    [matheo@storage ~]$ sudo vgcreate storage /dev/sdb
    Volume group "storage" successfully created
    [matheo@storage ~]$ sudo vgextend storage /dev/sdc
    Volume group "storage" successfully extended  

créer un nouveau logical volume (LV) : ce sera la partition utilisable :

    [matheo@storage ~]$ sudo lvcreate -l 100%FREE storage -n LVstorage
    Logical volume "LVstorage" created.  

**🌞 Formater la partition**

    [matheo@storage ~]$ sudo mkfs -t ext4 /dev/storage/LVstorage
    Creating filesystem with 1046528 4k blocks and 261632 inodes
    Filesystem UUID: 40821ff2-1526-4703-8cdd-18e787c2f860
    Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912, 819200, 884736

    Allocating group tables: done
    Writing inode tables: done
    Creating journal (16384 blocks): done
    Writing superblocks and filesystem accounting information: done  

**🌞 Monter la partition**

montage de la partition (avec la commande mount)

    [matheo@storage ~]$ df -h
    /dev/mapper/storage-LVstorage  3.9G   24K  3.7G   1% /mnt/LVstorage1

définir un montage automatique de la partition (fichier /etc/fstab)  :      

    /dev/storage/LVstorage /mnt/LVstorage1 ext4 defaults 0 0




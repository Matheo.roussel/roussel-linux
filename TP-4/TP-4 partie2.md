# Partie 2 : Serveur de partage de fichiers

**🌞 Donnez les commandes réalisées sur le serveur NFS storage.tp4.linux**

    [matheo@storage ~]$ sudo cat /etc/exports
    /storage/site_web_1 10.3.1.14(rw,sync,no_subtree_check)
    /storage/site_web_2 10.3.1.14(rw,sync,no_subtree_check)  

    [matheo@web ~]$ df -h | grep storage
    10.3.1.13:/storage/site_web_2  6.2G  1.4G  4.9G  22% /var/www/site_web_2
    10.3.1.13:/storage/site_web_1  6.2G  1.4G  4.9G  22% /var/www/site_web_1  

**🌞 Donnez les commandes réalisées sur le client NFS web.tp4.linux**

    
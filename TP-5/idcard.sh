#!/bin/bash
#matheo
#04/03/24
machine_name=$(hostnamectl | grep Static | cut -d' ' -f4)
echo "Machine name : ${machine_name}"

source /etc/os-release
kernel_version=$(uname -r)
echo "OS ${PRETTY_NAME} and kernel version is ${kernel_version}"

ipdeupoints=$(ip a | grep -E '^2:' -A 2 | grep inet | tr -s ' ' | cut -d' ' -f3)
echo "IP : ${ipdeupoints}"

RAMtotal=$(free --mega | tr -s ' ' | grep Mem: | cut -d' ' -f2)
RAMfree=$(free --mega | tr -s ' ' | grep Mem: | cut -d' ' -f4)
echo "RAM : total : ${RAMfree}mo memory available on ${RAMtotal}mo total memory"

leftdeupoints=$(df / -h | grep / | tr -s ' ' | cut -d' ' -f4)
echo "Disk : ${leftdeupoints} space left"

echo "Top 5 processes by RAM usage :"
while read math_while; do

  prog=$(cut -d' ' -f1 <<< "$math_while" | cut -d'/' -f4)
  echo "  - $prog"

done <<< "$(ps -eo cmd= --sort=-%mem | head -n5)"


echo "Listenig ports :"
while read math_while2; do

  prog1=$(cut -d':' -f2 <<< "$math_while2" | cut -d' ' -f1)
  prog2=$(cut -d' ' -f1 <<< "$math_while2")
  prog3=$(cut -d'"' -f2 <<< "$math_while2")
  echo "  - $prog1 $prog2 : $prog3"

done <<< "$(sudo ss -tunlpH4 | tr -s ' ')"


echo -e "${BOLD}PATH directories :${NC}"
NIFS="${IFS}"
IFS=':'
for dir in $PATH
do  
  echo "  - ${dir}"
done
IFS="${NIFS}"


Rchat=$(curl --silent https://api.thecatapi.com/v1/images/search)
Nchat=$(cut -d'"' -f8 <<< "$Rchat")
LesDifferentTypeDeChat=$( cut -d'.' -f4 <<< "$Nchat")
echo "Here is your random cat (${LesDifferentTypeDeChat} file) : ${Nchat}"
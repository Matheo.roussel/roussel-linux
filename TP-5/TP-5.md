# Partie 1 : Script carte d'identité

``` bash
[matheo@TP-5-VM idcard]$ ./idcard.sh
Machine name : TP-5-VM
OS Rocky Linux 9.2 (Blue Onyx) and kernel version is 5.14.0-284.30.1.el9_2.x86_64
IP : 10.3.1.14/24
RAM : total : 113mo memory available on 796mo total memory
Disk : 4.5G space left
Top 5 processes by RAM usage :
  - .vscode-server
  - .vscode-server
  - .vscode-server
  - .vscode-server
  - python3
Listenig ports :
  - 323 udp : chronyd
  - 111 udp : rpcbind
  - 45883 tcp : code-019f4d1419
  - 22 tcp : sshd
  - 111 tcp : rpcbind
PATH directories :
  - /home/matheo/.vscode-server/cli/servers/Stable-019f4d1419fbc8219a181fab7892ebccf7ee29a2/server/bin/remote-cli
  - /home/matheo/.local/bin
  - /home/matheo/bin
  - /usr/local/bin
  - /usr/bin
  - /usr/local/sbin
  - /usr/sbin
Here is your random cat (jpg file) : https://cdn2.thecatapi.com/images/e1f.jpg
```

# II. Script youtube-dl

```bash
[matheo@TP-5-VM yt]$ ./yt.sh https://www.youtube.com/watch?v=hfRVs-Ygmdg
work in progress
Vidéo https://www.youtube.com/watch?v=hfRVs-Ygmdg was downloaded.
File path : ./downloads/Black screen for 2 seconds/Black screen for 2 seconds.mp4
```
```bash
[matheo@TP-5-VM yt]$ tree
.
├── downloads
│   ├── 1 second black screen video
│   │   ├── 1 second black screen video.mp4
│   │   └── description
│   ├── Black screen for 2 seconds
│   │   ├── Black screen for 2 seconds.mp4
│   │   └── description
│   ├── Dog Bark Sound Effect HD [No Copyright]
│   │   ├── description
│   │   └── Dog Bark Sound Effect HD [No Copyright].mp4
│   ├── maow
│   │   ├── description
│   │   └── maow.mp4
│   ├── Mario Jump Sound Effect
│   │   ├── description
│   │   └── Mario Jump Sound Effect.mp4
│   └── Pokemon Pikachu - Sound Effect for Editing
│       ├── description
│       └── Pokemon Pikachu - Sound Effect for Editing.mp4
└── yt.sh
```



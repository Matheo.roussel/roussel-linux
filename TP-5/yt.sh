#!/bin/bash
#matheo
#05/03/2024

echo 'work in progress'
youtube-dl -o './downloads/%(title)s/%(title)s.mp4' --write-description $1 &> /dev/null
find . -type f -name '*.description' -exec bash -c 'mv "$0" "$(dirname "$0")/description"' {} \;

path=$(find ./downloads -maxdepth 1 -mindepth 1 -type d -printf '%T@ %p\n' | sort -n | tail -n 1 | cut -d' ' -f2-)
pathmp4=$(find "${path}" -name '*.mp4')
echo "Vidéo $1 was downloaded."
echo "File path : ${pathmp4}"

